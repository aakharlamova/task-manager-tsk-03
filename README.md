# TASK MANAGER 

# DEVELOPER INFO

Name: Anastasia Kharlamova

E-MAIL: aakharlamova@tsconsulting.com

# SOFTWARE

* JDK 11.0.9.1

* Windows OS

# HARDWARE

* RAM 16Gb

* CPU i5

# RUN PROGRAM

```
java -jar ./task-manager.jar
```

# SCREENSHOTS

https://yadi.sk/d/xmpkOefAqFnOAQ?w=1
